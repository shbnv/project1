from setuptools import setup, find_packages

import physeffect

with open('requirements.txt') as f:
    requirements = f.readlines()

setup(name="physeffect",
      version=physeffect.__version__,
      author="",
      author_email="",
      url="",
      license="MIT License",
      packages=find_packages(),
      install_requires=requirements,
      classifiers=[
          "Development Status :: 3 - Alpha",
          "Environment :: Console",
          "License :: OSI Approved :: MIT License",
          "Programming Language :: Python :: 3 :: Only",
          "Topic :: Software Development :: Libraries :: Python Modules"
      ]
      )
