import re
from collections import defaultdict
from itertools import chain

from graphviz import Digraph
from scipy.cluster.hierarchy import linkage, fcluster
from scipy.spatial.distance import pdist

GROUPING_SPACE_REGEX = re.compile(r'([^\w_-]|[+])', re.UNICODE)


def tokenize(text, _split=GROUPING_SPACE_REGEX.split):
    """
    Split text into tokens. Don't split by a hyphen.
    Preserve punctuation, but not whitespaces.
    """
    return [t for t in _split(text) if t and not t.isspace()]


def group(iterable, compare_func=None):
    def _is_group(o):
        return isinstance(o, list)

    def _group(target, iterable):
        buffer = [target, ]
        for current in iterable:
            should_be_grouped = (not _is_group(current) and
                                 compare_func(target, current))
            if should_be_grouped:
                buffer.append(current)
            else:
                yield current
        yield buffer

    if compare_func is None:
        compare_func = lambda a, b: a == b

    while True:
        target = next(iterable)
        if _is_group(target):
            break
        iterable = _group(target, iterable)

    yield target
    yield from iterable


def cluster(iterable, key, t):
    def _flat(func):
        def _wrapper(a, b):
            a, = a
            b, = b
            return func(a, b)

        return _wrapper

    # fix
    key = _flat(key)
    iterable = [[x] for x in iterable]

    dicts = pdist(iterable, key)
    dict_mtx = linkage(dicts, method='complete')
    labels = fcluster(dict_mtx, t, criterion='distance')

    # revert fix
    iterable = chain(*iterable)

    clusters = defaultdict(list)
    for label, sao in zip(labels, iterable):
        clusters[label].append(sao)

    return {k - 1: v for k, v in clusters.items()}


def visualize(tree, extension='png'):
    graph = Digraph()

    for x in tree:
        graph.node(name=str(x.id),
                   label='{}\n{} ({})\n{}'.format(x.id, x.form, x.lemma, x.upostag))

    for x in tree:
        if x.parent:
            graph.edge(head_name=str(x.id),
                       tail_name=str(x.parent.id),
                       label=x.deprel)
    return graph.pipe(extension)
