import re


def match_all(pattern, text):
    return tuple(pattern.finditer(text))


class PhasePattern:
    def __init__(self, *pattern):
        self.variants = list(re.compile(p, re.I) for p in pattern)

    def find(self, text):
        matches = self.find_variants(text)
        matches = (x for v in matches for x in v)
        return tuple(matches)

    def find_variants(self, text):
        matches = map(lambda x: self._match_all(x, text), self.variants)
        return tuple(matches)

    @staticmethod
    def _match_all(pattern, text):
        matches = pattern.finditer(text)
        matches = map(lambda x: (x.span(), x.group()), matches)
        return tuple(matches)


class Effect:
    def __init__(self, name, invalue, outvalue, obj):
        self.name = name
        self._components = [invalue, outvalue, obj]

    def find(self, text):
        matches = tuple(map(lambda x: x.find(text), self._components))
        k = sum(map(bool, matches)) / len(matches)
        return k, matches


def markup(text, spans):
    open_tag, close_tag = '<b>', '</b>'

    def _insert(string, entry, index):
        return string[:index] + entry + string[index:]

    offset = 0
    for span in spans:
        start, end = span
        text = _insert(text, open_tag, offset + start)
        offset += len(open_tag)
        text = _insert(text, close_tag, offset + end)
        offset += len(close_tag)

    return text


VOZ1_FIZVEL_1 = (
    r'voltage',
    r'(electric(al)?\s?)?potential\sdifference',
    r'electric(al)?\spressure',
    r'electric(al)?\stension',
)

VOZ10_FIZVEL_5 = (
    r'(electric(al)?\s?)?current',
)

E1 = (
    r'(semi)?conductor',
    r'resistance',
)
E2 = (r'metal\sconductor',)
E3 = (r'electrolyte',)
E4 = (r'mixed\sconductor',)
E5 = (r'superconductor',)
E6 = (r'semiconductor',)
E7 = (r'П/ПРОВ.С СОБС.ПРОВ',)
E8 = (r'П/ПР.С НЕСОБС.ПРОВ',)
E9 = (r'(dielectric|non-conductor)',)
E10 = (r'piezoelectric',)
E11 = (r'piroelectric',)
E12 = (r'segnetoelectric',)

F1 = (r'gas',)

VOZ1_FIZVEL_1 = PhasePattern(*VOZ1_FIZVEL_1)
VOZ10_FIZVEL_5 = PhasePattern(*VOZ10_FIZVEL_5)
E1 = PhasePattern(*E1)

effect37 = Effect('Закон Ома', VOZ1_FIZVEL_1, VOZ10_FIZVEL_5, E1)
