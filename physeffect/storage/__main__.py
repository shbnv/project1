import argparse

from physeffect.storage.bulk import Bulk
from physeffect.storage.parser import UsPatentApplication44Parser as UsptoParser
from physeffect.storage.reader import UsptoReader

BULK_PATH = './bulk2'

bulk = Bulk(BULK_PATH, UsptoReader(), UsptoParser())

parser = argparse.ArgumentParser()
parser.add_argument('files', nargs='+', help='*.xml or *.xml.zip')
args = parser.parse_args()

for file in args.files:
    bulk.put(file)
