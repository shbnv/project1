import os
from zipfile import ZipFile


class UsptoReader:
    """
    Reads XML-files, concatenated XML-files separated by XML_SEPARATOR,
    and also the same files compressed into the ZIP-archive.
    """
    XML_SEPARATOR = '<?xml version="1.0" encoding="UTF-8"?>\n'

    def read(self, *files):
        for file in files:
            _, ext = os.path.splitext(file)
            if ext == '.xml':
                yield from self._handle_xml_file(file)
            elif ext == '.zip':
                yield from self._handle_zipped_xml_files(file)
            else:
                # TODO: handle exception like 'file extension is invalid'
                pass

    def _split_xml_text(self, text):
        chunks = text.split(self.XML_SEPARATOR)
        documents = (self.XML_SEPARATOR + chunk.strip()
                     for chunk in chunks if chunk)
        yield from documents

    def _slice_xml_text(self, text):
        start = text.find(self.XML_SEPARATOR)
        while start > -1:
            end = text.find(self.XML_SEPARATOR, start + 1)
            if end > -1:
                yield text[start:end]
                start = end
            else:
                end = len(text)
                if len(text[start:end]) > len(self.XML_SEPARATOR):
                    yield text[start:end]
                break

    def _handle_xml_file(self, file):
        with open(file) as f:
            yield from self._slice_xml_text(f.read())

    def _handle_zipped_xml_files(self, file):
        archive = ZipFile(file)
        files = archive.namelist()
        # handle only xml-files
        files = [f for f in files if f.endswith(".xml")]
        for f in files:
            with archive.open(f) as xml:
                text = xml.read().decode()
                # Fix line separator error
                text = os.linesep.join(text.splitlines())
                yield from self._slice_xml_text(text)
