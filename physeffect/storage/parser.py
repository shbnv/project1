from abc import abstractmethod
from dataclasses import dataclass
from itertools import dropwhile, takewhile
from xml.etree import ElementTree


@dataclass
class UsPatentApplication:
    title: str
    number: str
    cpc: str
    abstract: str
    description: str
    summary: str
    claims: str

    def __repr__(self):
        return f'Patent(number={self.number}, ...)'

    def __hash__(self):
        return hash(self.number)


class Parser:
    @classmethod
    @abstractmethod
    def parse(cls, text): pass

    @classmethod
    @abstractmethod
    def is_valid(cls, text): pass


class UsPatentApplicationParser(Parser):
    _DOC_TYPE = 'us-patent-application'
    _DOC_VERSION = None
    _DOC_LANG = 'en'

    @classmethod
    def is_valid(cls, text):
        required = cls._DOC_TYPE, cls._DOC_VERSION, cls._DOC_LANG

        try:
            xml = ElementTree.fromstring(text)
            fetched = (xml.attrib['id'].lower(),
                       xml.attrib['dtd-version'].lower(),
                       xml.attrib['lang'].lower())
        except:
            # TODO: implement more useful exception handling
            return False

        if fetched and fetched == required:
            return True
        return False


class UsPatentApplication44Parser(UsPatentApplicationParser):
    _DOC_VERSION = 'v4.4 2014-04-03'

    @classmethod
    def parse(cls, text):
        try:
            xml = ElementTree.fromstring(text)

            title = (xml
                     .find('us-bibliographic-data-application')
                     .find('invention-title')
                     .text)

            number = (xml
                      .find('us-bibliographic-data-application')
                      .find('publication-reference')
                      .find('document-id')
                      .find('doc-number')
                      .text)

            abstract = ''.join(xml.find('abstract').itertext()).strip()
            description = ''.join(xml.find('description').itertext()).strip()

            def fetch_cpc(element):
                tags = ('section', 'class', 'subclass', 'main-group',
                        'subgroup', 'symbol-position', 'classification-value')
                return ''.join(element.find(x).text for x in tags)

            main_cpc = fetch_cpc((xml
                                  .find('us-bibliographic-data-application')
                                  .find('classifications-cpc')
                                  .find('main-cpc')
                                  .find('classification-cpc')))

            summary = dropwhile(lambda x: x.text.upper() != 'SUMMARY OF THE INVENTION',
                                xml.find('description'))
            next(summary)  # skip summary header
            paragraphs = takewhile(lambda x: x.tag == 'p', summary)
            summary = '\n'.join(p.text for p in paragraphs)

            def fetch_claim(claim):
                return ''.join(claim.itertext()).replace('\n', '')

            claims = '\n'.join(fetch_claim(claim) for claim in xml.find('claims'))

            return UsPatentApplication(title, number, main_cpc, abstract, description, summary, claims)

        except:
            # TODO: implement more useful exception handling
            return None
