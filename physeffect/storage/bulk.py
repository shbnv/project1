import os
from pickle import dump, load
from shutil import rmtree

from physeffect.storage.parser import UsPatentApplication44Parser as UsptoParser
from physeffect.storage.reader import UsptoReader


class Bulk:
    def __init__(self, root_path, reader, parser):
        self._root_path = root_path
        self._reader = reader
        self._parser = parser

    def put(self, file, override=False):
        patents = ((self._parser.parse(text), text)
                   for text in self._reader.read(file))
        for patent in patents:
            patent, raw_xml = patent

            if patent is None:
                continue

            if not len(patent.summary):
                continue

            new_path = os.path.join(self._root_path, patent.number)
            if os.path.exists(new_path):
                if override:
                    rmtree(new_path)
                    print(f'{new_path} already exists, it will be overridden!')
                else:
                    print(f'{new_path} already exists, it will be skipped!')
                    continue

            os.mkdir(new_path)

            with open(os.path.join(new_path, 'raw.xml'), 'w') as f:
                f.write(raw_xml)

            with open(os.path.join(new_path, 'patent.pickle'), 'wb') as f:
                dump(patent, f)

    def get(self, number):
        path = os.path.join(self._root_path, number, 'patent.pickle')
        return load(path)

    def remove(self, number):
        path = os.path.join(self._root_path, number)
        try:
            print(path)
            rmtree(path, ignore_errors=True)
        except:
            pass

    def patents(self):
        patents = (os.path.join(e.path, 'patent.pickle')
                   for e in os.scandir(self._root_path)
                   if e.is_dir() and not e.name.startswith('.'))
        for patent in patents:
            with open(patent, 'rb') as f:
                yield load(f)


class DefaultBulk(Bulk):
    def __init__(self, root_path):
        super().__init__(root_path, UsptoReader(), UsptoParser())
