import re
from collections import defaultdict
from itertools import islice, chain

from physeffect.storage.bulk import DefaultBulk


class Effect:
    def __init__(self, name, inp, out, obj):
        self.name = name
        self.in_value_regex = inp
        self.out_value_regex = out
        self.obj_regex = obj

    def __str__(self):
        return f'{self.__class__.__name__}(name={self.name}, ...)'

    def find(self, text):
        targets = (self.in_value_regex,
                   self.out_value_regex,
                   self.obj_regex)

        matches = tuple(self._match_all(t, text) for t in targets)

        is_matched = tuple(map(bool, matches))
        score = sum(is_matched) / len(is_matched)

        return score, tuple(chain(*matches))

    @staticmethod
    def _match_all(regex, text):
        if regex is None:
            return ()
        pattern = re.compile(regex, re.I)
        return tuple(pattern.finditer(text))


patterns = defaultdict(lambda: None)
patterns['VOZ1_FIZVEL_1'] = r'electric (pressure|tension|(field\s(strength|intensity)))'
patterns['VOZ1_FIZVEL_2'] = r'potential\sdifference'
patterns['VOZ1_FIZVEL_3'] = r'electromotive\sforce'
patterns['VOZ2_FIZVEL_1'] = r'magnetic\sinduction'
patterns['VOZ2_FIZVEL_3'] = r'magnetic\sflux'
patterns['VOZ4_FIZVEL_1'] = r'(force|power)\s(?:difference)?'
patterns['VOZ4_FIZVEL_2'] = r'pressure\s(?:difference)'
patterns['VOZ7_FIZVEL_4'] = r'particle\senergy'
patterns['VOZ9_FIZVEL_6'] = r'bulk\s(density\sof\sparticles|particle\sdensity)'
patterns['VOZ10_FIZVEL_1'] = r'(?:electric(al)?\s?)?\scurrent\sstrength'
patterns['VOZ10_FIZVEL_5'] = r'(?:electric(al)?\s?)?\s(current\sdensity)'
patterns['VOZ11_FIZVEL_R1_18'] = r'relative\sdeformation'
patterns['VOZ11_FIZVEL_R1_19'] = r'relative\s(volume|bulk)'
patterns['VOZ11_FIZVEL_R1_4'] = r'area'
patterns['VOZ11_FIZVEL_R2_1'] = r'temperature'
patterns['VOZ11_FIZVEL_R3_1'] = r'(?:electric(al)?\s?)?charge'
patterns['VOZ11_FIZVEL_R3_2'] = r'surface\selectric\scharge'
patterns['VOZ11_FIZVEL_R3_7'] = r'(?:electric(al)?\s?)?capacity'
effect37 = Effect('Закон Ома', patterns['VOZ1_FIZVEL_1'], patterns['VOZ10_FIZVEL_5'], 'conductor')
BULK_PATH = '/mnt/DATA/patents/bulk'

if __name__ == '__main__':
    bulk = DefaultBulk(BULK_PATH)
    patents = islice(bulk.patents(), 1000)

    foo = []
    for patent in patents:
        value = effect37.find(patent.description)
        foo.append((patent.number, value))
    from pprint import pprint
    foo = list(filter(lambda x: float(x[0]) > 0.5, foo))
    pprint(foo)
