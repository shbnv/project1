import re
from xml.etree import ElementTree as etree

UDPIPE_MODEL = './udpipe/english-ewt-ud-2.3-181115.udpipe'


def iter_p(p):
    if p.tag == 'b':
        # pass
        if p.text: yield p.text
    elif p.tag == 'figref':
        yield 'FIGURES' if 'FIGS.' in p.text else 'FIGURE'
    else:
        if p.text:
            yield p.text

    for child in list(p):
        yield from iter_p(child)
        if child.tail:
            yield child.tail


def fix_spaces(string):
    string = string.strip()
    # remove multiple spaces
    string = re.sub(r'\s+', ' ', string)
    # remove space before punctuation, e.g. 'Lorem ipsum .'
    string = re.sub(r'\s+([.,:;!?])', r'\1', string)
    return string


if __name__ == '__main__':
    filename = './bulk/20170000001/raw.xml'

    xml = etree.parse(filename)
    description = xml.find('description')

    paragraphs = list(description)

    itertext = ' '.join(' '.join(p.itertext()) for p in paragraphs if p.tag != 'heading')
    itertext = fix_spaces(itertext)
    print('ITERTEXT:', itertext)
