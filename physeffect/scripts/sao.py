from collections import defaultdict, Counter
from itertools import chain, islice

import matplotlib.pyplot as plt
import numpy as np
import scipy
import seaborn as sns
from diskcache import FanoutCache

from physeffect.misc import Timer
from physeffect.semtools.misc import cluster
from physeffect.semtools.models import SAO
from physeffect.semtools.udpipe import UDPipeParser
from physeffect.storage.bulk import DefaultBulk
from sklearn.decomposition import TruncatedSVD

BULK_PATH = './bulk'
MODEL = './udpipe/english-ewt-ud-2.3-181115.udpipe'
cache = FanoutCache('./.cache/physeffect')


def heatmap(mtx, filename=None):
    fig, ax = plt.subplots(figsize=(15, 15))
    # sns.heatmap(np.array(mtx), cmap=sns.cubehelix_palette(8, start=.5, rot=-.75))
    sns.heatmap(np.array(mtx), cmap='Blues')
    plt.yticks(rotation=0, fontsize=16)
    plt.xticks(fontsize=12)
    plt.tight_layout()
    if filename:
        plt.savefig(filename)
    else:
        plt.show()


def histogram(mtx, filename=None):
    h = sorted([sum(row) for row in mtx])

    fit = scipy.stats.norm.pdf(h, np.mean(h), np.std(h))
    plt.plot(h, fit, '-o')
    plt.hist(h, density=True)
    # plt.savefig(f'{datetime.datetime.now()}.png')
    if filename:
        plt.savefig(filename)
    else:
        plt.show()


@cache.memoize()
def extract_sao(text):
    trees = parser.parse(text)
    sao_list = chain(*map(SAO.extract, trees))
    return tuple(sao_list)


# @cache.memoize()
def sao_distance(a, b):
    return 1 - a.compare(b)


if __name__ == '__main__':
    bulk = DefaultBulk(BULK_PATH)
    parser = UDPipeParser(MODEL)
    timer = Timer()

    patents = islice(bulk.patents(), 100)
    patents = tuple(patents)

    pat_sao = defaultdict(list)
    for patent in patents:
        sao_list = extract_sao(patent.summary)
        pat_sao[patent].extend(sao_list)

    timer.step('extract sao')

    sao_pat = defaultdict(list)
    for pat, sao_list in pat_sao.items():
        for sao in sao_list:
            sao_pat[sao].append(pat)

    timer.step('sao-patent')

    clusters = cluster(sao_pat.keys(), sao_distance, 0.8)

    timer.step('clustering')

    sao_group = defaultdict(int)
    for group, sao_list in clusters.items():
        for sao in sao_list:
            sao_group[sao] = group

    timer.step('sao-group')

    gsao_pat = defaultdict(list)
    for sao, pat_list in sao_pat.items():
        group = sao_group[sao]
        counter = Counter(pat_list)
        gsao_pat[group].append([counter[x] for x in patents])

    for group in gsao_pat:
        gsao_pat[group] = list(map(sum, zip(*gsao_pat[group])))

    timer.step('gsao-patent')

    heatmap(list(gsao_pat.values()))
