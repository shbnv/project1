from physeffect.semtools.misc import visualize
from physeffect.semtools.udpipe import UDPipeParser
from physeffect.semtools.models import SAO
from uuid import uuid4
import os

MODEL = './udpipe/english-ewt-ud-2.3-181115.udpipe'
path = 'pics'
parser = UDPipeParser(MODEL)

tree, = list(parser.parse(text))
sao_list = list(SAO.extract(tree))

new_dir = os.path.join(path, str(uuid4()))
print(new_dir)
os.mkdir(new_dir)

p = os.path.join(new_dir, 'tree.png')
with open(p, 'wb') as f:
    f.write(visualize(tree))

for index, sao in enumerate(sao_list):
    p = os.path.join(new_dir, f'{index}')
    with open(p + '.png', 'wb') as f:
        f.write(visualize(sao._tree))
    with open(p + '.txt', 'w') as f:
        f.write(str(sao._tree))
