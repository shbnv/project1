from peewee import *
from functools import reduce
from itertools import islice
import re

legacy_db = SqliteDatabase('./database/PhysicalEffects.sqlite')


class FEIndex(Model):
    IDFE = IntegerField()
    INDX = CharField(null=True)

    class Meta:
        primary_key = False
        database = legacy_db


def compose(*funcs):
    return lambda x: reduce(lambda f, g: g(f), list(funcs), x)


def get_fizvel(index):
    input, output = [], []
    rows = index.split('\n')
    try:
        for row in rows:
            number, *contents = row.split(':')
            number = int(number)
            for content in contents:
                if number == 0:
                    items = set(re.split(',|;', content))
                    result = list(filter(lambda x: 'FIZVEL' in x, items))
                    input.append(result)
                elif number == 1:
                    items = set(re.split(',|;', content))
                    result = list(filter(lambda x: 'FIZVEL' in x, items))
                    output.append(result)
    except:
        return None
    return input, output


def flatten(l):
    return [l] if not isinstance(l, list) else [x for X in l for x in flatten(X)]


if __name__ == '__main__':
    legacy_db.connect()
    encode = lambda x: x.encode('ascii')
    colon = lambda x: x.replace(b'\x02\x03\x04', b':')
    fuck = lambda x: x.replace(b'\x02\x02', b':')
    semicolon = lambda x: x.replace(b'\x03\x04', b';')
    comma = lambda x: x.replace(b'\x04', b',')
    line_break = lambda x: x.replace(b'\x01', b'\n')
    decode = lambda x: x.decode('utf-8')
    strip = lambda x: x.strip()

    cleanup = compose(encode, colon, fuck, semicolon, comma, line_break, decode, strip)

    indices = (cleanup(x.INDX) for x in FEIndex.select())
    indices = list(indices)

    z = list(filter(None.__ne__, (get_fizvel(x) for x in indices)))
    z = [list(x) for x in z]
    for _ in z:
        print(set(flatten(_)))
