import os
import re
from itertools import islice

from physeffect.storage.bulk import Bulk
from physeffect.storage.parser import UsPatentApplication44Parser as UsptoParser
from physeffect.storage.reader import UsptoReader


def match_all(pattern, text):
    return tuple(pattern.finditer(text))


VOZ1_FIZVEL_1_REGEX = r'(?:electric(al)?\s?)?(pressure|tension|(field\s(strength|intensity)))'
VOZ1_FIZVEL_1_PATTERN = re.compile(VOZ1_FIZVEL_1_REGEX, re.I)

VOZ10_FIZVEL_5_REGEX = r'(?:electric(al)?\s?)?(current\sdensity)'
VOZ10_FIZVEL_5_PATTERN = re.compile(VOZ10_FIZVEL_5_REGEX, re.I)

CONDUCTOR_REGEX = r'conductor'
CONDUCTOR_PATTERN = re.compile(CONDUCTOR_REGEX, re.I)

BULK_PATH = os.path.join(PROJECT_HOME, 'bulk')


class EffectPattern:
    def __init__(self, object, input, output):
        self._object_pattern = object
        self._input_pattern = input
        self._output_pattern = output

    def find(self, text):
        targets = (self._object_pattern, self._input_pattern, self._output_pattern)
        targets_matches = [bool(match_all(t, text)) for t in targets]
        a, b, c = targets_matches

    @staticmethod
    def _match_all(pattern, text):
        return tuple(pattern.finditer(text))


class PhysicalEffect:
    def __init__(self):
        self._full_in_effect = None
        self._full_out_effect = None
        self._obj = None
        self._partial_in_effect = None
        self._partial_out_effect = None

    def find(self, text):
        pass


if __name__ == '__main__':
    bulk = Bulk(BULK_PATH, UsptoReader(), UsptoParser())
    patents = islice(bulk.patents(), 1000)
    phe37 = EffectPattern(CONDUCTOR_PATTERN, VOZ1_FIZVEL_1_PATTERN, VOZ10_FIZVEL_5_PATTERN)

    for patent in patents:
        text = patent.summary.replace('\n', ' ')

        k = phe37.find(text)
        k = int(bool(k))
        print(patent.number, k)
