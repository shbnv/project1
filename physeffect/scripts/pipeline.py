from pprint import pprint
from collections import defaultdict, Counter
from itertools import chain, islice

from diskcache import FanoutCache

from physeffect.misc import Timer
from physeffect.semtools.misc import cluster
from physeffect.semtools.models import SAO
from physeffect.semtools.udpipe import UDPipeParser
from physeffect.storage.bulk import DefaultBulk
from collections import defaultdict, Counter
from itertools import chain, islice

import matplotlib.pyplot as plt
import numpy as np
import scipy
import seaborn as sns
from diskcache import FanoutCache

from physeffect.misc import Timer
from physeffect.semtools.misc import cluster
from physeffect.semtools.models import SAO
from physeffect.semtools.udpipe import UDPipeParser
from physeffect.storage.bulk import DefaultBulk
from sklearn.decomposition import TruncatedSVD

BULK_PATH = './patents/bulk2'
MODEL = './udpipe/english-ewt-ud-2.3-181115.udpipe'
cache = FanoutCache('./.cache/physeffect')


@cache.memoize()
def extract_sao(text):
    trees = parser.parse(text)
    sao_list = chain(*map(SAO.extract, trees))
    return tuple(sao_list)


def sao_distance(a, b):
    return 1 - a.compare(b)


def heatmap(mtx, filename=None):
    fig, ax = plt.subplots(figsize=(15, 15))
    # sns.heatmap(np.array(mtx), cmap=sns.cubehelix_palette(8, start=.5, rot=-.75))
    sns.heatmap(np.array(mtx), cmap='Blues')
    plt.yticks(rotation=0, fontsize=16)
    plt.xticks(fontsize=12)
    plt.tight_layout()
    if filename:
        plt.savefig(filename)
    else:
        plt.show()


if __name__ == '__main__':
    bulk = DefaultBulk(BULK_PATH)
    parser = UDPipeParser(MODEL)
    timer = Timer()

    patents = bulk.patents()
    patents = islice(patents, 200)
    patents = tuple(patents)

    timer.step('load patents')

    pat_sao = defaultdict(list)
    for patent in patents:
        sao_list = extract_sao(patent.summary)
        pat_sao[patent].extend(sao_list)

    total_sao_list = list(chain(*pat_sao.values()))

    timer.step('extract sao')
    print('SAO count:', len(total_sao_list))

    clusters = cluster(total_sao_list, sao_distance, 0.3)

    timer.step('clustering')

    sao_group = {sao: group
                 for group, sao_list in clusters.items()
                 for sao in sao_list}

    timer.step('sao_group')

    pat_gsao = defaultdict(list)
    for pat, sao_list in pat_sao.items():
        groups = [sao_group[sao] for sao in sao_list]
        pat_gsao[pat] = groups

    timer.step('pat_gsao')

    groups_count = len(clusters.keys())
    print('GROUPS_COUNT:', groups_count)
    pat_grouplist = defaultdict(list)

    for pat, group_list in pat_gsao.items():
        counter = Counter(group_list)
        pat_grouplist[pat] = [counter[index] for index in range(groups_count)]

    timer.step('pat_grouplist')

    tf_mtx = [groups for pat, groups in pat_grouplist.items()]

    tf = np.array(tf_mtx)
    idf = np.sum(np.where(tf > 0, 1, 0), axis=0) / len(patents)
    tf_idf = tf * idf

    timer.step('tf_idf')
    heatmap(tf_idf)

    mean = np.mean(tf_idf, axis=0)
    top_groups = sorted(enumerate(mean), key=lambda x: x[1], reverse=True)
    top_groups = top_groups[:int(len(mean) * 0.1)]
    for group, tfidf in top_groups:

        sao_list = clusters[group]
        print(f'GROUP: number={group}, size={len(sao_list)}, tf_idf={tfidf}:')
        for sao in sao_list:
            print(sao)
            print(sao._tree)
            print('-' * 100)

        print('#' * 100)

    to_save = [x[0] for x in top_groups]
    shrink = tf_idf[:, to_save]
    heatmap(shrink)
