import os
import re

from diskcache import FanoutCache
from nltk import sent_tokenize

from physeffect.semtools.effect import match_all, VOZ1_FIZVEL1, VOZ10_FIZVEL_5, E1
from physeffect.storage.bulk import Bulk
from physeffect.storage.parser import UsPatentApplication44Parser as UsptoParser
from physeffect.storage.reader import UsptoReader

BULK_PATH = './patents/bulk'

# NESTED_ROUND_BRACKETS_PATTERN = r'(\((?>[^()]+|(?1))*\))'  # for atomic groups use 'regex' package
NESTED_ROUND_BRACKETS_PATTERN = r'\(((?=(?P<tmp>[^()]+))(?P=tmp)|\([^()]*\))+\)'
cache = FanoutCache(os.path.join(BULK_PATH, '.cache'))


def normalize(text):
    paragraphs = text.split('\n')

    def sub_word(pattern, repl, string):
        return re.sub(rf'(\s)?{pattern}(\s)?', rf'\g<1>{repl}\g<2>', string)

    paragraphs = map(lambda x: sub_word(r'U\.S\.', 'US', x), paragraphs)
    paragraphs = map(lambda x: x.strip(), paragraphs)
    paragraphs = filter(lambda x: len(x) > 30, paragraphs)

    paragraphs = filter(lambda x: x[-1] == '.', paragraphs)

    # paragraphs = map(lambda x: re.sub(r'(\s)?\(.*\)*(\s)?', r'', x), paragraphs)
    paragraphs = map(lambda x: re.sub(NESTED_ROUND_BRACKETS_PATTERN, r'', x), paragraphs)
    paragraphs = map(lambda x: re.sub(r'(\s)?\[.*\](\s)?', r'', x), paragraphs)
    paragraphs = map(lambda x: x.replace(r'“', ''), paragraphs)
    paragraphs = map(lambda x: x.replace(r'”', ''), paragraphs)

    paragraphs = map(lambda x: x.strip(), paragraphs)
    paragraphs = sent_tokenize('\n'.join(paragraphs))
    paragraphs = filter(lambda x: len(x) > 30, paragraphs)

    return '\n'.join(paragraphs)


def chunk_text(text):
    sentences = sent_tokenize(text)
    chunk = []

    for sentence in sentences:
        if len(' '.join(chunk)) < 1000:
            chunk.append(sentence)
        else:
            yield ' '.join(chunk)
            chunk = []


@cache.memoize()
def contains(pattern, text):
    matches = match_all(pattern, text)
    return len(matches)


if __name__ == '__main__':
    bulk = Bulk(BULK_PATH, UsptoReader(), UsptoParser())

    patents = bulk.patents()
    # patents = islice(patents, 100)

    voz1_fizvel_1 = re.compile(VOZ1_FIZVEL1, re.I)
    voz10_fizvel_5 = re.compile(VOZ10_FIZVEL_5, re.I)
    e1 = re.compile(E1, re.I)

    for patent in patents:
        text = normalize(patent.description)
        for index, paragraph in enumerate(chunk_text(text)):
            input_matches = contains(voz1_fizvel_1, paragraph)
            output_matches = contains(voz10_fizvel_5, paragraph)
            object_matches = contains(e1, paragraph)
            triplet = (input_matches, output_matches, object_matches)
            if sum(bool(x) for x in triplet) > 1:
                print(f'{patent.number}[{index}]: {input_matches}, {output_matches}, {object_matches}')
